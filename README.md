# CopyServer

A simple, python-based server to copy text from one device to another.

## Usage

***THIS SERVER IS INSECURE. DO NOT EXPOSE IT TO THE INTERNET.***

- Clone this repo
- Change PORT and URL to values that can be resolved by local clients
- Build and deploy the docker container, or run `autorun.sh`
