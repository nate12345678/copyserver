import http.server
import socketserver
import unicodedata

PORT = 8080
URL = f"http://server.local:{PORT}"

copyVal = ""

# For simplicity, keep the webpage as a string (slightly minified)
html = '''<!DOCTYPE html>
<html>
<head>
<title>Copy/Paste</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
<script>
function p() {
var i = document.getElementById("t").value;
var x = new XMLHttpRequest();
x.open("POST", "%s", true);
x.setRequestHeader("Content-Type", "text/plain");
x.onreadystatechange = function() {
if (x.readyState === 4 && x.status === 200) {
console.log("Data sent successfully!");
location.reload();
}
};
x.send(i);
}
</script>
<style>
body {background-color: #404050;}
p {font-size: large;color: white;}
textarea {font-size: large;width: 100%%;background-color: #505060;border-color: black;color: white;}
button {background-color: #202030;font-size: large;padding: 2px;margin: 5px;color: white;border-color: black;}
</style>
</head>
<body>
<p>%s</p><br>
<textarea id="t" rows=8 placeholder="paste"></textarea><br>
<button onclick="p()">paste</button>
</body>
</html>'''

def remove_control_characters(s: str) -> str:
    return "".join(ch for ch in s if unicodedata.category(ch)[0]!="C" or ch == '\n' or ch == '\t')

def convert_to_html(s: str) -> str:
    s = s.replace("<", "&lt")
    s = s.replace("&", "&amp")
    return s.replace("\n", "<br>")

class MHandler(http.server.BaseHTTPRequestHandler):

    def do_GET(self):
        global copyVal, html, PORT, URL
        print("get request on ", self.path)
        self.send_response(200)
        self.send_header("Content-Length", str(len(html)))
        self.end_headers()
        self.wfile.write(bytes(html % (URL, convert_to_html(copyVal)), "utf-8"))


    def do_POST(self):
        global copyVal
        content_length = int(self.headers['Content-Length'])
        s = remove_control_characters(self.rfile.read(content_length).decode("utf-8"))
        copyVal = s
        self.send_response(200)
        self.end_headers()

with socketserver.TCPServer(("", PORT), MHandler) as httpd:
    print("serving at port", PORT)
    httpd.serve_forever()
